"""
The script will create embeddings based on provided data using default "text-embedding-ada-002" model.
"""
import os
import openai
from pathlib import Path
from dotenv import load_dotenv
from langchain.chat_models import ChatOpenAI
from llama_index.node_parser import SimpleNodeParser
from llama_index.langchain_helpers.text_splitter import TokenTextSplitter
from llama_index import OpenAIEmbedding, PromptHelper, SimpleDirectoryReader
from llama_index import VectorStoreIndex, LLMPredictor, ServiceContext, set_global_service_context


# global level settings ------------------------------------------------------------------------------------------------

# load .env variable data
load_dotenv()

# get the ChatGPT API key
openai.api_key = os.getenv("OPENAI_API_KEY")

# set the model to be used later for predicting using word embeddings that we are going to create
llm = ChatOpenAI(model_name='gpt-3.5-turbo', temperature=0.7)
llm_predictor = LLMPredictor(llm=llm)

# Default embedding "text-embedding-ada-002" from OpenAI
embed_model = OpenAIEmbedding()

# to convert docs in chunks i.e. nodes
node_parser = SimpleNodeParser(text_splitter=TokenTextSplitter(chunk_size=1024, chunk_overlap=20))

#  helps with truncating and repacking text chunks to fit in the LLM’s context window
prompt_helper = PromptHelper(context_window=4096, num_output=256, chunk_overlap_ratio=0.1, chunk_size_limit=None)

# configure service context
service_context = ServiceContext.from_defaults(
  llm_predictor=llm_predictor,
  embed_model=embed_model,
  node_parser=node_parser,
  prompt_helper=prompt_helper
)

# set global variables so no need to use/mention at every stage later
set_global_service_context(service_context)

# docs path to read the data from
read_docs_path = Path("docs")

# storage path to store the embeddings
vec_embed_store_path = Path("vec_embed_data")

# ----------------------------------------------------------------------------------------------------------------------


def get_index(docs_path: Path, storage_path: Path) -> VectorStoreIndex:

    """
    This function will read the document and return word embeddings/ vector representation of the docs.
    Also, it will save these embeddings at specified location.

    Parameters
    ----------

    docs_path: Path
        The path where documents to read reside

    storage_path: Path
        The path to store vector embedding representation of the docs

    """

    # read data
    documents = SimpleDirectoryReader(docs_path).load_data()

    # convert to node
    parser = SimpleNodeParser()
    nodes = parser.get_nodes_from_documents(documents)

    # crate index based on the details
    index = VectorStoreIndex(nodes, service_context=service_context)

    # store the index at specified location
    index.storage_context.persist(persist_dir=storage_path)

    return index


if __name__ == "__main__":
    get_index(read_docs_path, vec_embed_store_path)
