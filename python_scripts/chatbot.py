"""
The script will create play ground to test chatbot
"""
import os
import openai
import gradio as gr
from pathlib import Path
from dotenv import load_dotenv
from llama_index import StorageContext, load_index_from_storage

# global level settings ------------------------------------------------------------------------------------------------

# load .env variable data
load_dotenv()

# get the ChatGPT API key
openai.api_key = os.getenv("OPENAI_API_KEY")

# storage path where embeddings have been stored
vec_embed_store_path = Path("vec_embed_data")

# ----------------------------------------------------------------------------------------------------------------------


def chatbot(input_text: str) -> str:

    """
    This function will provide the answer of the queries. Here first we will load the stored

    Parameters
    ----------

    input_text: str
        User's question

    """

    # rebuild storage context
    storage_context = StorageContext.from_defaults(persist_dir=vec_embed_store_path)

    # load index
    index = load_index_from_storage(storage_context)
    query_engine = index.as_query_engine()
    response = query_engine.query(input_text)
    return response


iface = gr.Interface(fn=chatbot,
                     inputs=gr.components.Textbox(lines=7, label="Enter your text"),
                     outputs="text",
                     title="Nayan's Chatbot")


iface.launch(share=True)
