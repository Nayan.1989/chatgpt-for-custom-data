# Steps:

1. Clone the git repo.
2. Go to the folder chatgpt-for-custom-data >> python_scripts
   * Windows
     ```bash
     cd chatgpt-for-custom-data\python_scripts
     ``` 
   * Linux
     ```bash
     cd chatgpt-for-custom-data/python_scripts
     ``` 
   * create an .env file.
   * Add a parameter OPENAI_API_KEY=YOUR_OPENAPI_KEY
3. Go to the folder chatgpt-for-custom-data
   * Windows
     ```bash
     cd ..\
     ``` 
   * Linux
     ```bash
     cd ../
     ``` 
4. Create a virtual environment.
   ```bash
   virtualenv venv
   ```
5. Activate the virtual environment.
   * Linux
      ```bash
      source venv/bin/activate
      ```
   * Windows
      ```bash
      venv\Scripts\activate
      ```
6. Install the required packages.
   ```bash
   pip install -r requirements.txt 
   ```
7. Create `docs` folder and put files in this folder.
   * I have personally used .pdf, .csv, .md, .txt files in the same folder, and it has worked quite well.
8. Create embeddings using ChatGPT model.
   * Linux
      ```bash
      python python_scripts/embeddings_method.py 
      ```
   * Windows
      ```bash
      python python_scripts\embeddings_method.py 
      ```
   * This will create embedding files in the directory vec_embed_data.
   * In the next step these embedding files will be used by ChatGPT model to provide responses.
9. Open the playground and test ChatGPT on custom data.
   * Linux
      ```bash
      python python_scripts/chatbot.py 
      ```
   * Windows
      ```bash
      python python_scripts\chatbot.py 
      ```
   * This will provide a local URL and a public URL which can be shared with others.
   * Visit any of the URLs in your browser and have conversation with ChatGPT on your custom data.